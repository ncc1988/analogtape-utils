# analogtape-utils

This is a collection of scripts for creating media compilations for compact cassette and VHS tapes.


## Script descriptions

### encode_vhs.sh: A script to help you make a video compilation for a VHS tape

encode_vhs.sh converts several input video files to one big video file so that you can easily record an entire VHS tape at once. It applies audio normalisation on each input video file and adds a 5 second pause (black image) between each video in the output file. More input file processing might be added in the future.

__Dependencies:__ ffmpeg (with libvorbis and libtheora support), normalize or normalize-audio, make

If your ffmpeg doesn't support encoding via libvorbis and/or libtheora, you may want to set a different codec in the parts of the script that build ffmpeg commands.

__Basic usage:__ encode_vhs.sh input_file1 input_file2 ...

You may also encode in grayscale by specifying the grayscale option: encode_vhs.sh --grayscale input_file1 input_file2 ...

__Configuration:__ There is no configuration at the moment, but you can change some variables at the top of the script. You might be interested in changing the "norm" variable from "pal" to "ntsc", if you prefer recording a VHS tape in NTSC instead of PAL/SECAM. This will also adjust the "resolution" and "fps" variables.

__How it works:__ encode_vhs.sh creates a temporary directory in the directory where it is invoked. Then, it creates a Makefile for the whole video processing job and starts make, running multiple jobs simultaneously when running on a system with several CPUs. The advantage of using makefiles for this is that dependency management and tracking of changes are built-in features of make so that these things don't have to be rewritten in the shell script. After make returned with success, the directory will contain a file called vhs.mkv which is the output file containing all input videos.
