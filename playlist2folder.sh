#!/bin/bash
#
#    playlist2folder.sh
#    Copyright (C) 2018-2019  Moritz Strohm <ncc1988@posteo.de>
#
#    This file is part of analogtape-utils
#
#    compact-cassette-tools is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    compact-cassette-tools is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with moeripper.  If not, see <http://www.gnu.org/licenses/>.
#


IFS='
'

i=1

playlist_file="$1"
prefix="$2"


if [ "$playlist_file" = '' ]
then
    echo "Usage: playlist2folder.sh PLAYLIST_FILE [PREFIX]"
    echo "       PLAYLIST_FILE is a cmus playlist file containing absolute file paths."
    echo "       PREFIX is an optional prefix that is prependen to the file number."
    echo
    echo "The files in the playlist files are copied into the current folder."
    echo "A number (and the optional prefix) is prepended to the file name"
    echo "to keep the order from the playlist intact."
    echo "Furthermore, a metadata file (named PREFIX-meta.txt) is created using"
    echo "exiftool. The metadata file contains the file name, artist and title"
    echo "of each file from the playlist."
    exit 0
fi

if [ ! -e "$playlist_file" ]
then
    echo "Playlist file not found!"
    exit 1
fi

echo "Copying playlist files..."

for f in $( cat "$playlist_file" )
do
    basename=$( basename "$f" )
    name=$( printf "%s%02d %s" "$prefix" $i "$basename")
    cp -v "$f" "$name"
    if [ "$prefix" = "" ]
    then
        exiftool -csv -Artist -Title "$name" | tail -n 1 >> "meta.csv"
    else
        exiftool -csv -Artist -Title "$name" | tail -n 1 >> "meta-$prefix.csv"
    fi
    i=$( expr $i + 1 )
done

echo "Playlist files copied!"
