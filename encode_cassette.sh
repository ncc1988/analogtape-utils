#!/bin/bash
#
#    encode_cassette.sh
#    Copyright (C) 2018-2019  Moritz Strohm <ncc1988@posteo.de>
#
#    This file is part of analogtape-utils
#
#    compact-cassette-tools is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    compact-cassette-tools is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with moeripper.  If not, see <http://www.gnu.org/licenses/>.
#




#NOTE: This script only works with ffmpeg installations
#that have libmp3lame or libvorbis support enabled.


use_normalize=1

#encode_in_vorbis is set to false by default for compatibility with
#MP3 players and MP3 CD-Players that don't support OGG Vorbis.
encode_in_vorbis=0
mp3_bitrate=320
vorbis_bitrate=320
player_pitch=1.0
no_normalize=0


show_help()
{
    echo -e "Usage: encode_cassette.sh [--mode=SPEED_MODE] [--player-pitch=PITCH] [--no-normalize] INPUT FILE"
    echo -e "\tSPEED MODE can be used to record in different speeds than standard speed"
    echo -e "\tusing a tape recorder that can only record in standard speed."
    echo -e "\tSetting this parameter is optional. When it is set, it must be set to one of"
    echo -e "\tthe following modes:"
    echo -e "\t\tsp\tStandard speed"
    echo -e "\t\tlp1\tLong play mode 1 (half standard speed)"
    echo -e "\t\tlp2\tLong play mode 2 (quarter standard speed)"
    echo -e "\tPITCH is a factor for fine-adjusting the recording speed."
    echo -e "\tThis is useful, if the cassette recorder records cassettes too slow"
    echo -e "\tor too fast. Examples:"
    echo -e "\t\tSet PITCH to 1.05, if your recorder is running 5 percent too fast."
    echo -e "\t\tSet PITCH to 0.98, if your recorder is running 3 percent too slow."
    echo -e "\tINPUT FILE must be a media file that can be converted by ffmpeg."
    exit 0
}


lp_mode=0
input_files=() #An array of input files

if [ $# -ge 2 ]
then
    all_args_read=0
    #Get all parameters:
    while [ $# -gt 0 ]
    do
        if [ $all_args_read -eq 0 ]
        then
            arg=$( echo "$1" | cut -d= -f1 )
            val=$( echo "$1" | cut -d= -f2 )
            if [ $arg == '--mode' ]
            then
                if [ $val == 'sp' ]
                then
                    lp_mode=0
                elif [ $val == 'lp1' ]
                then
                    lp_mode=1
                elif [ $val == 'lp2' ]
                then
                    lp_mode=2
                fi
            elif [ $arg == '--player-pitch' ]
            then
                player_pitch=$val
            elif [ $arg == '--no-normalize' ]
            then
                no_normalize=1
            else
                all_args_read=1
            fi
        fi
        if [ $all_args_read -eq 1 ]
        then
            input_files+=("$1")
        fi
        shift
    done
elif [ $# -eq 1 ]
then
    if [ $1 == '--help' ]
    then
        show_help
    fi
    #Get the file name:
    input_files+=("$1")
else
    show_help
fi


if [ $lp_mode -gt 0 -o $( echo "$player_pitch == 1.00" | bc) -ne 1 ]
then
    #Speed adjustment is requested. We need exiftool for that.
    command -v exiftool 2>&1 > /dev/null
    if [ $? -eq 1 ]
    then
        #exiftool not installed.
        echo "The program exiftool is not installed."
        echo "Cannot convert files to long play modes or do pitch adjustments!"
        exit 1
    fi
fi

if [ $no_normalize -lt 1 ]
then
    command -v normalize 2>&1 > /dev/null
    if [ $? -eq 1 ]
    then
        #normalize is not installed. Use normalize-audio instead.
        echo "The program normalize is not installed. Using normalize-audio instead!"
        use_normalize=0
    fi
fi


if [ $lp_mode -eq 0 ]
then
    echo "Encoding in standard speed!"
elif [ $lp_mode -eq 1 ]
then
    echo "Encoding for LP1 using a recorder that records in standard speed!"
elif [ $lp_mode -eq 2 ]
then
    echo "Encoding for LP2 using a recorder that records in standard speed!"
else
    echo "Unsupported recording speed mode!"
    exit 1
fi


IFS='
'

#Add a newline before the real action starts:
echo


for f in ${input_files[@]}
do
    echo "$f"
    metadata=$(exiftool -S -t -Artist -Title -TrackNumber -Album -License "$f")
    artist=$(echo "$metadata" | cut -f1 )
    title=$(echo "$metadata" | cut -f2 )
    track_number=$(echo "$metadata" | cut -f3 )
    album=$(echo "$metadata" | cut -f4 )
    license=$(echo "$metadata" | cut -f5 )

    #We must determine the samplerate using exiftool:
    rate=$(exiftool -SampleRate "$f" | cut -d: -f2 | tr -d ' ')
    if [ ! $rate ]
    then
        rate=$(exiftool -AudioSampleRate "$f" | cut -d: -f2 | tr -d ' ')
        if [ ! $rate ]
        then
            echo "Cannot determine rate!"
            exit 1
        fi
    fi
    read_rate=''
    if [ $lp_mode -gt 0 ]
    then
        #A long play mode is used on a standard play recorder:
        #The converted audio file must be in double speed (LP1) or 4x speed (LP2).
        #Furthermore, player pitch must be regarded.
        read_rate=$( echo "scale = 0; ($rate * $lp_mode * 2 * $player_pitch)/1" | bc )
    else
        #Standard speed on a standard play recorder:
        #Only regard player pitch.
        read_rate=$( echo "scale = 0; ($rate * $player_pitch)/1" | bc )
    fi

    if [ $read_rate == '' ]
    then
        echo "Cannot calculate read rate!"
        exit 1
    fi
    if [ $read_rate -eq $rate ]
    then
       #No long play mode and no player pitch.
       #We can just read the file in the normal samplerate.
       echo "$f" | grep ".wav$" 2>&1 > /dev/null
       if [ $? -eq 0 ]
       then
           #The file is already a wav file.
           #Rename the wav file so that it can be addresses as $f.wav
           #like the decoded files.
           cp "$f" "$f.wav"
       else
           #The file is not a wav file: decode it.
           nice -n 19 ffmpeg -i "$f" -vn -codec:a pcm_s16le -y "$f.wav"
       fi
    else
        #Read the file with a different samplerate.
        nice -n 19 ffmpeg -i "$f" -vn -codec:a pcm_s16le -filter asetrate="$read_rate"\
             -ar "$rate" -y "$f.wav"
    fi

    #Normalise the file:

    if [ $no_normalize -lt 1 ]
    then
        if [ $use_normalize -eq 1 ]
        then
            normalize "$f.wav"
        else
            normalize-audio "$f.wav"
        fi
    fi

    #Encode the file. If that is successful remove the temporary wav file.
    #If the temporary wav file has been removed remove the original file.
    if [ $encode_in_vorbis -eq 1 ]
    then
        new_file_name="${f%.*}.ogg"
        nice -n 19 ffmpeg -i "$f.wav" -vn -codec:a libvorbis -b:a "$vorbis_bitrate"k\
           -metadata title="$title" -metadata artist="$artist"\
           -metadata album="$album" -metadata track="$track_number"\
           -metadata license="$license" -y "$new_file_name"\
            && rm "$f.wav"
    else
        new_file_name="${f%.*}.mp3"
        nice -n 19 ffmpeg -i "$f.wav" -vn -codec:a libmp3lame -b:a "$mp3_bitrate"k\
               -metadata title="$title" -metadata artist="$artist"\
               -metadata album="$album" -metadata track="$track_number"\
               -metadata license="$license" -y "$new_file_name"\
            && rm "$f.wav"
    fi
done
