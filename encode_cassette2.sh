#!/bin/bash
#
#    encode_cassette2.sh
#    This file is part of analogtape-utils.
#    Copyright (C) 2019-2020  Moritz Strohm <ncc1988@posteo.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with moeripper.  If not, see <http://www.gnu.org/licenses/>.
#

#NOTE: This script requires ffmpeg with support for the flac audio format.


#Hard-coded default configuration.
#See encode_cassette.config.sh for an explaination of the parameters.
#TODO: write configuration file with explaination!

pause_duration=1
normalise_audio=1
process_niceness=19

#Try to load a custom configuration from ~/.config/analogtape-utils/encode_cassette.config.sh:
. ~/.config/analogtape-utils/encode_cassette.config.sh 2> /dev/null


#Program starts here

#Calculate the maximum amount of parallel tasks for make:
num_processes=$( nproc )
if [ $num_processes -lt 1 ]
then
    num_processes=1
fi

unsorted_input_files=()
std_normalize_cmd=1

show_help()
{
    echo -e "Usage: encode_cassette.sh [--no-normalise] [-pause PAUSE_DURATION] INPUT FILES\n\n"
    exit 0
}


IFS='
'

if [ $# -ge 1 ]
then
    if [ $1 == '--help' ]
    then
        show_help
    elif [ $1 == '--no-normalise' ]
    then
        normalise_audio=0
        shift
    elif [ $1 == '-pause' ]
    then
        shift
        pause_duration="$1"
        shift
    fi

    #Get all other parameters which contain the file names:
    while [ $# -gt 0 ]
    do
        unsorted_input_files+=("$1")
        shift
    done
elif [ $# -eq 1 ]
then
    if [ $1 == '--help' ]
    then
        show_help
    fi
    #Get the file name:
    unsorted_input_files+=("$1")
else
    show_help
fi

if [ $normalise_audio -gt 0 ]
then
    command -v normalize 2>&1 > /dev/null
    if [ $? -eq 1 ]
    then
        #normalize is not installed. Use normalize-audio instead.
        echo "The program normalize is not installed. Using normalize-audio instead!"
        std_normalize_cmd=0
    fi
else
    std_normalize_cmd=-1
fi


tmpdir="./encode_cassette.tmp"
concat_file_name="encode_cassette.concat.list"
mkdir "$tmpdir"
echo "ffconcat version 1.0" > "$tmpdir/$concat_file_name"
if [ $pause_duration -gt 0 ]
then
    #Start with a $pause_duration second delay:
    echo "file 'silence.wav'" >> "$tmpdir/$concat_file_name"
fi

if [ ${#unsorted_input_files[@]} -lt 1 ]
then
    echo "No input file(s) specified!"
    exit 0
fi

makefile_name="./$tmpdir/Makefile"

#Generate makefile:
track_target_names="tracks ="
track_targets=""

#sort the input files:
input_files=$( printf "%s\n" "${unsorted_input_files[@]}" | sort )

i=1
for f in ${input_files[@]}
do
    echo "$f"
    output_base_filename="track_$i"
    ln "$f" "./$tmpdir/$output_base_filename.input"
    target="$output_base_filename.wav: $output_base_filename.input"$'\n'$'\t'
    track_target_names="$track_target_names $output_base_filename.wav"

    target="$target ffmpeg -v 24 -i \"$output_base_filename.input\" -vn\
             -codec:a pcm_s16le -ac 2 -ar 44100 \
             -y \"$output_base_filename.wav\""$'\n'$'\t'
    if [ $std_normalize_cmd -gt -1 ]
    then
        if [ $std_normalize_cmd -eq 1 ]
        then
            target="$target normalize -q \"$output_base_filename.wav\""$'\n'
        else
            target="$target normalize-audio -q \"$output_base_filename.wav\""$'\n'
        fi
    fi

    #We must also add the targets to the ffmpeg concat file to get
    #one big audio file:
    escaped_output_filename=$( echo "$output_base_filename" | sed -e "s/'/\\\'/")
    echo "file '$escaped_output_filename.wav'" >> "$tmpdir/$concat_file_name"
    if [ $pause_duration -gt 0 ]
    then
        echo "file 'silence.wav'" >> "$tmpdir/$concat_file_name"
    fi

    track_targets="$track_targets"$'\n'"$target"$'\n'
    i=$( expr $i + 1 )
done
echo "$track_target_names" > "$makefile_name"

if [ $pause_duration -gt 0 ]
then
    main_target='side.flac: $(tracks) silence.wav'$'\n'$'\t'"ffmpeg -v 24 -safe 0 -f concat -i \"$concat_file_name\" -codec:a flac -y side.flac"
else
    main_target='side.flac: $(tracks) '$'\n'$'\t'"ffmpeg -v 24 -safe 0 -f concat -i \"$concat_file_name\" -codec:a flac -y side.flac"
fi
echo "$main_target" >> "$makefile_name"

echo "$track_targets" >> "$makefile_name"

if [ $pause_duration -gt 0 ]
then
    silence_target='silence.wav: '$'\n'$'\t'
    silence_target="$silence_target ffmpeg -v 24 -f lavfi -i \"anullsrc=r=44100:cl=stereo\" \
         -t $pause_duration -codec:a pcm_s16le -ar 44100 \
         -y \"silence.wav\""$'\n'
    echo "$silence_target" >> "$makefile_name"
fi

cd "$tmpdir"

nice -n "$process_niceness" make -j"$num_processes"

if [ $? -eq 0 ]
then
    mv "side.flac" ../
fi

cd ..

rm -rf "$tmpdir"

echo "encode_cassette.sh has compiled all selected audio tracks into one file!"
