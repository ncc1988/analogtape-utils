#!/bin/bash
#
#    encode_vhs.sh
#    This file is part of analogtape-utils.
#    Copyright (C) 2019  Moritz Strohm <ncc1988@posteo.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with moeripper.  If not, see <http://www.gnu.org/licenses/>.
#

#NOTE: This script requires ffmpeg with support for libtheora and libvorbis
#encoding into a matroska file.


#Hard-coded default configuration.
#See encode_vhs.config.sh for an explaination of the parameters.

pause_duration=5
norm='pal'
grayscale=0
normalise_audio=1
process_niceness=19

#Try to load a custom configuration from ~/.config/analogtape-utils/encode_vhs.config.sh:
. ~/.config/analogtape-utils/encode_vhs.config.sh 2> /dev/null


#Program starts here

#Calculate the maximum amount of parallel tasks for make:
num_processes=$( nproc )
if [ $num_processes -lt 1 ]
then
    num_processes=1
fi

input_files=()
std_normalize_cmd=1
if [ $normalise_audio -gt 0 ]
then
    command -v normalize 2>&1 > /dev/null
    if [ $? -eq 1 ]
    then
        #normalize is not installed. Use normalize-audio instead.
        echo "The program normalize is not installed. Using normalize-audio instead!"
        std_normalize_cmd=0
    fi
else
    std_normalize_cmd=-1
fi


show_help()
{
    echo -e "Usage: encode_vhs.sh [--grayscale] INPUT FILES\n\n"
    exit 0
}


IFS='
'

if [ $# -ge 1 ]
then
    if [ $1 == '--help' ]
    then
        show_help
    elif [ $1 == '--grayscale' ]
    then
        grayscale=1
        shift
    fi

    #Get all other parameters which contain the file names:
    while [ $# -gt 0 ]
    do
        input_files+=("$1")
        shift
    done
elif [ $# -eq 1 ]
then
    if [ $1 == '--help' ]
    then
        show_help
    fi
    #Get the file name:
    input_files+=("$1")
else
    show_help
fi


resolution='768x576'
fps='25'
if [ $norm = 'ntsc' ]
then
    resolution='640x480'
    fps='29.97'
fi

tmpdir="./encode_vhs.tmp"
concat_file_name="encode_vhs.concat.list"
mkdir "$tmpdir"
echo "ffconcat version 1.0" > "$tmpdir/$concat_file_name"
#Start with a $pause_duration second delay:
echo "file 'black.mkv'" >> "$tmpdir/$concat_file_name"

if [ ${#input_files[@]} -lt 1 ]
then
    echo "No input file(s) specified!"
    exit 0
fi

makefile_name="./$tmpdir/Makefile"

#Generate makefile:
track_target_names="tracks ="
track_targets=""

i=1
for f in ${input_files[@]}
do
    output_base_filename="track_$i"
    ln "$f" "./$tmpdir/$output_base_filename.input"
    target="$output_base_filename.mkv: $output_base_filename.input"$'\n'$'\t'
    track_target_names="$track_target_names $output_base_filename.mkv"

    if [ $grayscale -eq 1 ]
    then
        target="$target ffmpeg -v 24 -i \"$output_base_filename.input\" -s \"$resolution\" -r \"$fps\" \
             -codec:v libtheora -b:v 4000k  -vf \"hue=s=0\" \
             -an -y \"$output_base_filename.video.mkv\" \
             -codec:a pcm_s16le -ac 2 -ar 48000 -async 1 \
             -vn -y \"$output_base_filename.audio.wav\""$'\n'$'\t'
    else
        target="$target ffmpeg -v 24 -i \"$output_base_filename.input\" -s \"$resolution\" -r \"$fps\" \
             -codec:v libtheora -b:v 4000k \
             -an -y \"$output_base_filename.video.mkv\" \
             -codec:a pcm_s16le -ac 2 -ar 48000 -async 1 \
             -vn -y \"$output_base_filename.audio.wav\""$'\n'$'\t'
    fi
    if [ $std_normalize_cmd -gt -1 ]
    then
        if [ $std_normalize_cmd -eq 1 ]
        then
            target="$target normalize -q \"$output_base_filename.audio.wav\""$'\n'$'\t'
        else
            target="$target normalize-audio -q \"$output_base_filename.audio.wav\""$'\n'$'\t'
        fi
    fi

    target="$target ffmpeg -v 0 -i \"$output_base_filename.video.mkv\" \
         -i \"$output_base_filename.audio.wav\" \
         -codec:v copy -codec:a libvorbis -b:a 320k \
         -y \"$output_base_filename.mkv\""$'\n'

    #We must also add the targets to the ffmpeg concat file to get
    #one big video file:
    escaped_output_filename=$( echo "$output_base_filename" | sed -e "s/'/\\\'/")
    echo "file '$escaped_output_filename.mkv'" >> "$tmpdir/$concat_file_name"
    echo "file 'black.mkv'" >> "$tmpdir/$concat_file_name"

    track_targets="$track_targets"$'\n'"$target"$'\n'
    i=$( expr $i + 1 )
done
echo "$track_target_names" > "$makefile_name"

main_target='vhs.mkv: $(tracks) black.mkv'$'\n'"\
"$'\t'"ffmpeg -v 24 -safe 0 -f concat -i \"$concat_file_name\"\
       -codec:v copy -codec:a copy -y vhs.mkv"
echo "$main_target" >> "$makefile_name"

echo "$track_targets" >> "$makefile_name"

black_target='black.mkv: '$'\n'$'\t'
if [ $grayscale -eq 1 ]
then
    black_target="$black_target ffmpeg -v 24 -f lavfi -i \"color=c=black:s=$resolution:d=$pause_duration\" \
         -f lavfi -i \"anullsrc=r=48000:cl=stereo\" -vf \"hue=s=0\" -shortest \
         -r \"$fps\" -codec:v libtheora -b:v 4000k \
         -codec:a libvorbis -b:a 320k \
         -y \"black.mkv\""$'\n'
else
    black_target="$black_target ffmpeg -v 24 -f lavfi -i \"color=c=black:s=$resolution:d=$pause_duration\" \
         -f lavfi -i \"anullsrc=r=48000:cl=stereo\" -shortest \
         -r \"$fps\" -codec:v libtheora -b:v 4000k \
         -codec:a libvorbis -b:a 320k \
         -y \"black.mkv\""$'\n'
fi
echo "$black_target" >> "$makefile_name"

cd "$tmpdir"

nice -n "$process_niceness" make -j"$num_processes"

if [ $? -eq 0 ]
then
    mv "vhs.mkv" ../
fi

cd ..

echo "encode_vhs.sh has finished encoding video for VHS recording!"
