#!/bin/bash


# This is the configuration file for encode_vhs.sh.
# Copy it to ~/.config/analogtape-utils/encode_vhs.config.sh
# and change the variables below as you like.


# The amount of seconds with a black picture that is added before
# the first track, after the last track and between tracks.
pause_duration=5

# The video norm to be used. This also sets the framerate and resolution.
# The value "pal" (for PAL/SECAM) sets the framerate to 25 FPS with
# a resolution of 768x576.
# The value "ntsc" will set the framerate to 29.97 FPS with a resolution
# of 640x480. The default is "pal".
norm='pal'

# Whether to enforce grayscale encoding (1) or not (0). This may be handy
# if you don't want to specify the --grayscale parameter everytime when
# calling encode_vhs.sh.
grayscale=0

# Wheter to enforce audio normalisation (1) or not (0).
normalise_audio=1

# The "niceness" level at which the encoding process shall run.
# The value 0 is the lowest allowed for non-root users and represents the
# highest priority. Higher values (e.g. 19) mean lower priority.
process_niceness=19
